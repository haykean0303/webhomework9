import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { Navbar, NavDropdown, Form, Button, FormControl, Nav } from "react-bootstrap";

export default function MyNav() {
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#">React-Router</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="mr-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Nav.Link href="#action1">Home</Nav.Link>
            <Nav.Link href="#action2">Video</Nav.Link>
            <Nav.Link href="#action2">Account</Nav.Link>
            <Nav.Link href="#action2">Welcome</Nav.Link>
            <Nav.Link href="#action2">Auth</Nav.Link>

          </Nav>
          <Form className="d-flex">
            <FormControl
              type="search"
              placeholder="Search"
              className="mr-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}
