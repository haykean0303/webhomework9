import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import MyNav from "./page/MyNav";
import { Container } from "react-bootstrap";
import { BrowserRouter} from "react-router-dom";

function App() {
  return (
    <div>
      <Container>
        <BrowserRouter>
          <MyNav />
        </BrowserRouter>
      </Container>
    </div>
  );
}

export default App;
